cmake_minimum_required(VERSION 3.9)

project(paper-football-engine)

if(NOT DEFINED CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE DEBUG)
endif(NOT DEFINED CMAKE_BUILD_TYPE)

#---------->INCLUDE ADDITIONAL CMAKE MODULES AND MACROS<--------------

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake_modules)
include(CheckIncludeFiles)
include(CheckFunctionExists)
include(CheckLibraryExists)
include(${PROJECT_SOURCE_DIR}/cmake_modules/FindHeaderDirectories.cmake)

#-------------------------->SANITIZER<--------------------------------

if(DO_SATITIZE MATCHES YES)
set(CMAKE_EXE_LINKER_FLAGS "-fsanitize=address ${CMAKE_EXE_LINKER_FLAGS}")
add_definitions(-fno-omit-frame-pointer -fsanitize=address)
endif(DO_SATITIZE MATCHES YES)

#-------------------------->ENVIRONMENT<-----------------------------

set(CMAKE_INCLUDE_CURRENT_DIR ON)

#find_package(Threads REQUIRED) #find package Threads(libpthread for Linux)

#--------------------------->HEADERS AND SOURCES<-------------------------

FIND_HEADER_DIRECTORIES(INC_DIRS ./include)
set(SRC_DIR ./sources)
set(INC_DIR ./include)
set(BIN_DIR ./bin)

file(GLOB_RECURSE SRC_LIST FOLLOW_SYMLINKS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "${SRC_DIR}/*.c")  #find *.c in SRC_DIR
file(GLOB_RECURSE INC_LIST FOLLOW_SYMLINKS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "${INC_DIR}/*.h")  #find *.h in INC_DIR

set(INC_LIST ${INC_LIST})

include_directories(${INC_DIRS})

add_library(${PROJECT_NAME} SHARED ${SRC_LIST} ${INC_LIST})

add_executable(${PROJECT_NAME}_BIN ${SRC_LIST} ${INC_LIST})

target_link_libraries(${PROJECT_NAME} m)
target_link_libraries(${PROJECT_NAME}_BIN m)

set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 98)
