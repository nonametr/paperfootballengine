#ifdef __WIN64
#define UNITY_EXPORT extern __declspec(dllexport)
#else
#define UNITY_EXPORT extern
#endif

UNITY_EXPORT struct geometry * create_std_geometry(const int width, const int height, const int goal_width);
UNITY_EXPORT struct geometry * create_hockey_geometry(const int width, const int height, const int goal_width, const int depth);
UNITY_EXPORT void destroy_geometry(struct geometry * restrict const me);

UNITY_EXPORT struct ai* create_ai_storage();
UNITY_EXPORT void destroy_ai_storage(struct ai* storage);

UNITY_EXPORT const struct state * mcts_ai_get_state(const struct ai * const ai);

UNITY_EXPORT enum state_status state_status(const struct state * const me);
UNITY_EXPORT int state_get_ball(const struct state * const me);
UNITY_EXPORT int state_get_active(const struct state * const me);

UNITY_EXPORT int mcts_ai_undo_step(struct ai * restrict const ai);
UNITY_EXPORT int mcts_ai_reset(struct ai * restrict const ai, const struct geometry * const geometry);
UNITY_EXPORT int mcts_ai_do_step(struct ai * restrict const ai, const enum step step);
UNITY_EXPORT int mcts_ai_do_steps(struct ai * restrict const ai, const unsigned int qsteps, const enum step steps[]);
UNITY_EXPORT enum step mcts_ai_go(struct ai * restrict const ai, struct step_stat * restrict const stats);
UNITY_EXPORT const struct ai_param * mcts_ai_get_params(const struct ai * const ai);
UNITY_EXPORT int mcts_ai_set_param(struct ai * restrict const ai, const char * const name, const void * const value);
UNITY_EXPORT void free_mcts_ai(struct ai * restrict const ai);

UNITY_EXPORT int init_random_ai(struct ai * restrict const ai, const struct geometry * const geometry);
UNITY_EXPORT int init_mcts_ai(struct ai * restrict const ai, const struct geometry * const geometry);
