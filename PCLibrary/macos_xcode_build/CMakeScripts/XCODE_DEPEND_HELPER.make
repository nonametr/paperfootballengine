# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.paper-football-engine.Debug:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Debug/libpaper-football-engine.dylib:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Debug/libpaper-football-engine.dylib


PostBuild.paper-football-engine_BIN.Debug:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Debug/paper-football-engine_BIN:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Debug/paper-football-engine_BIN


PostBuild.paper-football-engine.Release:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Release/libpaper-football-engine.dylib:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Release/libpaper-football-engine.dylib


PostBuild.paper-football-engine_BIN.Release:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Release/paper-football-engine_BIN:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/Release/paper-football-engine_BIN


PostBuild.paper-football-engine.MinSizeRel:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/MinSizeRel/libpaper-football-engine.dylib:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/MinSizeRel/libpaper-football-engine.dylib


PostBuild.paper-football-engine_BIN.MinSizeRel:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/MinSizeRel/paper-football-engine_BIN:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/MinSizeRel/paper-football-engine_BIN


PostBuild.paper-football-engine.RelWithDebInfo:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/RelWithDebInfo/libpaper-football-engine.dylib:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/RelWithDebInfo/libpaper-football-engine.dylib


PostBuild.paper-football-engine_BIN.RelWithDebInfo:
/Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/RelWithDebInfo/paper-football-engine_BIN:
	/bin/rm -f /Users/nonametr/Projects/PaperFootballEngine/PCLibrary/osx_build/RelWithDebInfo/paper-football-engine_BIN




# For each target create a dummy ruleso the target does not have to exist
